package com.liondy.thymeleafexplore;

public class QRCode {
  public String name;
  public String image;

  public QRCode(String name, String image) {
    this.name = name;
    this.image = image;
  }
}
