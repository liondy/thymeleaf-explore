package com.liondy.thymeleafexplore;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.LinkedHashMap;

@Controller
public class EmployeesController {

  public LinkedHashMap<String,String>[] createEmployeeList() {
    LinkedHashMap<String,String> employee = new LinkedHashMap<>();
    LinkedHashMap<String,String>[] employees = new LinkedHashMap[5];
    for (int i = 0; i < employees.length; i++) {
      employee.put("First Name", "Asep");
      employee.put("Last Name", "Syarifuddin");
      employee.put("Address", "Cilacap");
      employees[i] = employee;
    }
    return employees;
  }

  @GetMapping("/employees")
  public String getEmployeesData(Model model) {
    model.addAttribute("designation","");
    model.addAttribute("subtitles","");
    model.addAttribute("referenceDate","2021-12-10, 14:24:48 Uhr");
    model.addAttribute("employees",createEmployeeList());
    return "exportEmployeeTemplates";
  }
}
