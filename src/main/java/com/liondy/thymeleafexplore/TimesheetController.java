package com.liondy.thymeleafexplore;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class TimesheetController {

  /*
  * contoh data:
  * [
  {
    month : 'yyyy-mm',
    subcontractorEmployeeName : String,
    exportDate : 'YYYY-MM-DD HH:MM:SS',
    workingHours : [
      {
        start :  'YYYY-MM-DD HH:MM:SS',
        end :  'YYYY-MM-DD HH:MM:SS'
      },
      {...}
    ],
    constructionSite : {
       projectNumber : String,
       projectTitle : String,
       projectAddress : String
    }
  }
]
  * */
  @GetMapping("/timesheet")
  public String exportTimesheet(Model model) {
    model.addAttribute("month", "December 2021");
    model.addAttribute("subcontractor", "Adi - Alfred");
    model.addAttribute("exportDate", "2021-12-14, 18:03:43 Uhr");
    model.addAttribute("rows", 31);
    return "exportTimesheetTemplates";
  }
}
