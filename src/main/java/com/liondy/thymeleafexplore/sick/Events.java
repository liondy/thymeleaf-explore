package com.liondy.thymeleafexplore.sick;

public class Events {
  private final String month;
  private final String[] sickLeaves;
  private final int totalSickLeaves;

  public Events(String month, String[] sickLeaves) {
    this.month = month;
    this.sickLeaves = sickLeaves;
    int total = 0;
    for (String event : sickLeaves) {
      if (event.equals("K")) total++;
    }
    this.totalSickLeaves = total;
  }

  public String getMonth() {
    return this.month;
  }

  public String[] getSickLeaves() {
    return this.sickLeaves;
  }

  public int getTotalSickLeaves() {
    return this.totalSickLeaves;
  }
}
