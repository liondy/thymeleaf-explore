package com.liondy.thymeleafexplore.sick;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.HashMap;

@Controller
public class SickLeaveController {

  private final String[] months = {"Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Des"};
  private final String[] events = {"F", "K", "W", "", "-"};

  private int getRandomNumber(int min, int max) {
    return (int) ((Math.random() * (max - min)) + min);
  }

  private Reports fillSickLeaveData(String name, int currentPage, int totalPage) {
    String employeeName = name;
    String exportDate = "2022.02.07 11.20.32";
    int year = 2021;
    Events[] monthlyReport = new Events[12];
    for (int i = 0; i < monthlyReport.length; i++) {
      String month = this.months[i];
      String[] sickLeaves = new String[31];
      for (int j = 0; j < sickLeaves.length; j++) {
        int idx = this.getRandomNumber(0,5);
        String event = this.events[idx];
        sickLeaves[j] = event;
      }
      monthlyReport[i] = new Events(month, sickLeaves);
    }
    return new Reports(employeeName, exportDate, year, monthlyReport, currentPage, totalPage);
  }
  
  private Reports[] createSickLeaveData(int totalPage){
    Reports[] data = new Reports[totalPage];
    for (int i = 0; i < totalPage; i++) {
      String name = "Asep Sudrajat";
      data[i] = this.fillSickLeaveData(name, (i+1), totalPage);
    }
    return data;
  }

  @GetMapping("/sick-leave")
  public String exportSickLeave(Model model) {
    model.addAttribute("data", this.createSickLeaveData(1));
    return "sickLeaveTemplates";
  }
}