package com.liondy.thymeleafexplore.sick;

public class Reports {
  private final String employeeName;
  private final String exportDate;
  private final int year;
  private final Events[] monthlyReport;
  private final int total;
  private final int currentPage;
  private final int totalPage;

  public Reports(String employeeName, String exportDate, int year, Events[] monthlyReport, int currentPage, int totalPage) {
    this.employeeName = employeeName;
    this.exportDate = exportDate;
    this.year = year;
    this.monthlyReport = monthlyReport;
    int totalSickInYear = 0;
    for (Events events : monthlyReport) {
      totalSickInYear += events.getTotalSickLeaves();
    }
    this.total = totalSickInYear;
    this.currentPage = currentPage;
    this.totalPage = totalPage;
  }

  public String getEmployeeName() {
    return employeeName;
  }

  public String getExportDate() {
    return this.exportDate;
  }

  public int getYear() {
    return this.year;
  }

  public Events[] getMonthlyReport() {
    return this.monthlyReport;
  }

  public int getTotal() {
    return this.total;
  }

  public int getCurrentPage() {
    return this.currentPage;
  }

  public int getTotalPage() {
    return this.totalPage;
  }
}
