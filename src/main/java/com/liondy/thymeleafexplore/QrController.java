package com.liondy.thymeleafexplore;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Random;

@Controller
public class QrController {

  private String generateQr() {
    Random random = new Random();
    String charSet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    String qr = "";
    for (int i = 0; i < 4; i++) {
      int idx = random.nextInt(charSet.length());
      char chara = charSet.charAt(idx);
      qr += chara;
    }
    return qr;
  }

  private QRCode[] getListOfQrCodes(int n) {
    QRCode[] qrCodes = new QRCode[n];
    for (int i = 0; i < qrCodes.length; i++) {
      String name = this.generateQr();
      qrCodes[i] = new QRCode(name, "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d0/QR_code_for_mobile_English_Wikipedia.svg/220px-QR_code_for_mobile_English_Wikipedia.svg.png");
    }
    return qrCodes;
  }

  @GetMapping
  public String getQrCodes(Model model) {
    int n = 15;
    model.addAttribute("qrcodes",this.getListOfQrCodes(n));
    model.addAttribute("rows", (int) Math.ceil((float)n/3)-1);
    return "qrTemplates";
  }
}
