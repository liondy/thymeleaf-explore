# Thymeleaf Exploration
Here is my thymeleaf exploration with Java Spring Boot Application. \
To initialize the Spring Boot Application, simply go to [https://start.spring.io/](https://start.spring.io/). \
Generate the Spring Boot Application using this configuration. \
![image](/uploads/8b0165d7fa06fbb13c0b993e912d15c7/image.png)

## Case Study
Try to generate QR Codes and display it in HTML files \
The problem is user wants to display it in rows with each rows contain only 3 qrcodes

## Java Solution
Create a QRCode Controller Class. This class has 3 methods:
1. private generateQR(): to generate 4 random characters in 1 QR Code
2. private getListOfQrCodes(int n): to get n list of qr codes. This method has one params that determine the length of qrcodes. For every qrcodes, it will call the generateQR() method.
3. public getQrCodes(Model model): this will be the public method and our mapper model. It will call the getListOfQrCodes() method with params n, and pass the result to template. It passes the number of rows to display in html because thymeleaf can't calculate ceiling.

## Thymeleaf Solution
The thymeleaf template has to be the same with the name of return in `getQrCodes()` method. \
I wrap it in HTML table so that each row contains max 3 qrCodes.
Below is the HTML Structure:
```html
<table>
    <tr>
        <td>img</td>
        <td>img</td>
        <td>img</td>
    </tr>
</table>
```
Below is the pseudocode:
```
for i from 0 until rows:
  for j from 0 until 2:
    if (i*3)+j < qrcodes.length
      print qrcodes[(i*3)+j]
    else print ''
```
this will give the result if n = 5:
```
qrcodes[0]  qrcodes[1]  qrcodes[2]
qrcodes[3]  qrcodes[4]  ''
```

## Result
- If n = 10 \
  ![image](/uploads/f9850b6c44917fd8447545d6e0250bf5/image.png)
- If n = 15 \
  ![image](/uploads/7836b8bb0187526aa84189b651d29fd6/image.png)